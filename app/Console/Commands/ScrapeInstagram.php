<?php

namespace App\Console\Commands;

use App\Models\Project;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Console\Command;

class ScrapeInstagram extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:instagram';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape instagram';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $driver = $this->createDriverConnection();
        $projects = Project::whereNotNull('instagram')->get();
        foreach ($projects as $project) {
            $driver->get($project->instagram);

            dump($driver->findElement(WebDriverBy::cssSelector('h1'))->getText());
            dump($driver->findElement(WebDriverBy::cssSelector('h1'))->getText() == 'Instagram');
            if ($driver->findElement(WebDriverBy::cssSelector('h1'))->getText() == 'Instagram') {
                dump('here we are');
                sleep(4);
                $inputs = $driver->findElements(WebDriverBy::cssSelector('input'));
                dump(count($inputs));
                if (count($inputs) > 1) {
                    dump('here we are1');
                    $inputs[0]->sendKeys(env('IG_EMAIL'));
                    $inputs[1]->sendKeys(env('IG_PASSWORD'));
                    sleep(1);
                    $driver->findElements(WebDriverBy::cssSelector('button'))[1]->click();
                    sleep(6);
                    $buttons = $driver->findElements(WebDriverBy::cssSelector('button'));
                    dump(count($buttons));
                    if (count($buttons) > 0 && $buttons[0]->getText() == 'Save information') {
                        dump('here we are2');
                        $buttons[0]->click();
                        sleep(6);
                    }

                    $buttons = $driver->findElements(WebDriverBy::cssSelector('button[tabindex]:not([role]):not([aria-label])'));
                    dump(count($buttons));
                    if (count($buttons) > 1 && $buttons[1]->getText() == 'Not Now') {
                        dump('here we are3');
                        $buttons[1]->click();
                    }

                    $driver->get($project->instagram);
                }
            }

            $followers = $driver->findElement(WebDriverBy::cssSelector('header section > div + ul > li:nth-child(2) span'))
                ->getAttribute('title');
            dump($followers);
            $followers = str_replace(' ', '', $followers);
            $followers = str_replace(',', '', $followers);
            dump($followers);
            $followers = intval($followers);
            $project->instagram_followers = $followers;
            $project->save();
        }

        $driver->quit();

        return Command::SUCCESS;
    }

    public function createDriverConnection()
    {
        $serverUrl = 'http://localhost:4444';
        $desiredCapabilities = DesiredCapabilities::chrome();
        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments(['headless']);
        $chromeOptions->addArguments(['--no-sandbox']);
        $chromeOptions->addArguments(['window-size=1920,1080']);
        $chromeOptions->addArguments(['--disable-dev-shm-usage']);
        $chromeOptions->addArguments(["--disable-system-timezone-automatic-detection", "--local-timezone"]);
        $chromeOptions->addArguments(['--verbose']);
        $chromeOptions->addArguments(["--whitelisted-ips=''"]);
        $chromeOptions->addArguments(['--disable-extensions']);
        $chromeOptions->addArguments(['--disable-gpu']);
        $chromeOptions->addArguments(['--enable-features=NetworkService,NetworkServiceInProcess']);
        $chromeOptions->addArguments(['user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36']);
        $desiredCapabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        $driver = RemoteWebDriver::create($serverUrl, $desiredCapabilities, 3600000,3600000);
//        $driver = RemoteWebDriver::create($serverUrl, DesiredCapabilities::chrome());

        return $driver;
    }
}
