<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Interactions\WebDriverActions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Console\Command;
use KubAT\PhpSimple\HtmlDomParser;
use App\Models\Project;


class ScrapeDonor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:donor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->scrapeDonor1();
        $this->scrapeDonor2();

        return Command::SUCCESS;
    }

    public function scrapeDonor2()
    {
        $driver = $this->createDriverConnection();
        $driver->get('https://coinmarketcap.com/nft/upcoming/');
        $projectsData = [];

        do {
            $projectsElements = $driver->findElements(WebDriverBy::cssSelector('tr'));
            $titles = $driver->findElements(WebDriverBy::cssSelector('td:first-child > div > p:first-child > span:first-child'));
            $currencies = $driver->findElements(WebDriverBy::cssSelector('td:first-child > div > p:first-child > span:last-child'));
            $descriptions = $driver->findElements(WebDriverBy::cssSelector('td:first-child > div > p:last-child'));
            $discords = $driver->findElements(WebDriverBy::cssSelector('td:nth-child(2) > div > p:first-child a'));
            $twitters = $driver->findElements(WebDriverBy::cssSelector('td:nth-child(2) > div > p:nth-child(2) a'));
            $websites = $driver->findElements(WebDriverBy::cssSelector('td:nth-child(2) > div > p:last-child a'));
            $starts = $driver->findElements(WebDriverBy::cssSelector('td:nth-child(3) > div > p:last-child'));
            $prices = $driver->findElements(WebDriverBy::cssSelector('td:nth-child(4) > div > span'));
            $pagesCounter = 0;
            foreach ($projectsElements as $key => $projectElement) {
                if ($key == 0) {
                    continue;
                }

                $title = $titles[$key - 1]->getText();
                $currency = $currencies[$key - 1]->getText();
                $description = $descriptions[$key - 1]->getText();
                $discord = $discords[$key - 1]->getAttribute('href');
                $twitter = $twitters[$key - 1]->getAttribute('href');
                $website = $websites[$key - 1]->getAttribute('href');
                $startsIn = $starts[$key - 1]->getText();
                $price = $prices[$key - 1]->getText();

//                $newDate = Carbon::parse($startsIn, 'Europe/Kiev');
                $newDate = Carbon::createFromFormat('d/m/Y, H:i:s', $startsIn, 'Europe/Kiev');
                $newDate->addHours(4);
                $newDate->setTimezone('UTC');

                // Add 2h to the date or 4h
                $date = $newDate->format('Y-m-d');
                $time = $newDate->format('H:i');

                if (strpos($price, 'Pre-sale') !== false) {
                    $price = $projectElement->findElement(WebDriverBy::cssSelector('td:nth-child(4) > div > span:last-child'))
                        ->getText();
                }

                $price = explode('Sale: ', $price)[1];
                switch ($currency) {
                    case 'Ethereum':
                        $price = explode(' ETH', $price)[0];
                        $currency = 'ETH';
                        break;

                    case 'Solana':
                        $price = explode(' SOL', $price)[0];
                        $currency = 'SOL';
                        break;
                }

                $projectsData[] = [
                    'title' => $title,
                    'description' => $description,
                    'discord' => $discord,
                    'twitter' => $twitter,
                    'website' => $website,
                    'date' => $date,
                    'time' => $time,
                    'price' => $price,
                ];

                $project = Project::whereTitle($title)->first();
                if (!$project) {
                    $project = new Project();
                }

                $project->title = $project->title != $title ? $title : $project->title;
                $project->link = $project->link != $website ? $website : $project->link;
                $project->discord = $project->discord != $discord ? $discord : $project->discord;
                $project->twitter = $project->twitter != $twitter ? $twitter : $project->twitter;
                $project->till_the_mint = $project->till_the_mint ?: null;
                $project->count = $project->count ?: null;
                $project->price = $project->price != floatval($price) ? floatval($price) : $project->price;
                $project->extras = $project->extras != $description ? $description : $project->extras;
                $project->date = $project->date != $date ? $date : $project->date;
                $project->time = $project->time != $time ? $time : $project->time;
                $project->currency = $currency;

                $project->save();

//            dump($title, $description, $discord, $twitter, $website, $startsIn, $date, $time, $price);
//                dump('Project ' . $key);
            }

            $nextPageEl = $driver->findElement(WebDriverBy::cssSelector('.pagination .next > a'));
            if ($nextPageEl->getAttribute('aria-disabled') == 'true') {
                break;
            }

            $driver->get('https://coinmarketcap.com' . $nextPageEl->getAttribute('href'));
//            dump("DONE Page $pagesCounter");
            sleep(3);
        } while (true);

        $driver->quit();
    }

    public function scrapeDonor1()
    {
        include './simple_html_dom.php';
        $driver = $this->createDriverConnection();
        $driver->get('https://howrare.is/drops');
        $blocks = $driver->findElements(WebDriverBy::cssSelector('.all_coll_row.drop_date'));
        foreach ($blocks as $blockKey => $block) {
            $currentDate = Carbon::now();
            $currentMonth = $currentDate->month;
            $year = $currentDate->year;
            $date = trim($block->getText()) != 'No date specified yet' ? trim($block->getText()) : null;
            $date = trim($block->getText()) != 'NO DATE SPECIFIED YET' ? trim($block->getText()) : null;
            $day = (int) filter_var($date, FILTER_SANITIZE_NUMBER_INT);
            $month = null;
            switch (true) {
                case str_contains($date, 'JANUARY'):
                    $month = 1;
                    break;
                case str_contains($date, 'FEBRUARY'):
                    $month = 2;
                    break;
                case str_contains($date, 'MARCH'):
                    $month = 3;
                    break;
                case str_contains($date, 'APRIL'):
                    $month = 4;
                    break;
                case str_contains($date, 'MAY'):
                    $month = 5;
                    break;
                case str_contains($date, 'JUNE'):
                    $month = 6;
                    break;
                case str_contains($date, 'JULY'):
                    $month = 7;
                    break;
                case str_contains($date, 'AUGUST'):
                    $month = 8;
                    break;
                case str_contains($date, 'SEPTEMBER'):
                    $month = 9;
                    break;
                case str_contains($date, 'OCTOBER'):
                    $month = 10;
                    break;
                case str_contains($date, 'NOVEMBER'):
                    $month = 11;
                    break;
                case str_contains($date, 'DECEMBER'):
                    $month = 12;
                    break;
            }

            if ($month < $currentMonth) {
                $year++;
            }

            $blockProjects = $driver->findElements(WebDriverBy::cssSelector(".all_collections_wrap.drops:nth-child(" . ($blockKey + 2) . ") .all_coll_row"));
            $isTillTheMintColumn = false;
            if ($driver->findElements(WebDriverBy::cssSelector(".all_coll_row:nth-child(2) > div:nth-child(4)"))[$blockKey]->getText() == 'TILL THE MINT') {
                $isTillTheMintColumn = true;
            }

            foreach ($blockProjects as $projectKey => $blockProject) {
                if ($projectKey > 1) {
                    $currentProjectSelector = ".all_collections_wrap.drops:nth-child(" . ($blockKey + 2) . ") .all_coll_row:nth-child(" . ($projectKey + 1) . ")";
                    $currentEl = $driver->findElements(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(1) span"));
                    if (!count($currentEl)) {
                        continue;
                    }

                    $title = $driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(1) span"))->getText();
                    $linkEl = $driver->findElements(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(2) > a:first-child"));
                    $link = count($linkEl) ? $linkEl[0]->getAttribute('href') : null;
                    $discordEl = $driver->findElements(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(2) > a:last-child"));
                    $discord = count($discordEl) ? $discordEl[0]->getAttribute('href') : null;
                    $twitterEl = $driver->findElements(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(2) > a:nth-child(2)"));
                    $twitter = count($twitterEl) ? $twitterEl[0]->getAttribute('href') : null;
                    switch (true) {
                        case strpos($link, 'discord.gg') !== false :
                            $discord = $link;
                            break;
                        case strpos($link, 'twitter.com') !== false :
                            $twitter = $link;
                            break;
                        case strpos($twitter, 'discord.gg') !== false :
                            $discord = $twitter;
                            break;
                    }

                    $link = strpos($link, 'discord.gg') !== false || strpos($link, 'twitter.com') !== false ? null : $link;
                    $twitter = strpos($twitter, 'twitter.com') !== false ? $twitter : null;
                    $discord = strpos($discord, 'discord.gg') !== false ? $discord : null;

                    $time = trim($driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(3)"))->getText());
                    $time = explode(' UTC', $time)[0];
                    if ($isTillTheMintColumn) {
                        $tillTheMint = $driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(4)"))->getText();
                        $count = trim($driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(5)"))->getText());
                        $price = trim($driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(6)"))->getText());
                        $extras = trim($driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(7)"))->getText());
                    } else {
                        $tillTheMint = null;
                        $count = trim($driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(4)"))->getText());
                        $price = trim($driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(5)"))->getText());
                        $extras = trim($driver->findElement(WebDriverBy::cssSelector($currentProjectSelector . " > div:nth-child(6)"))->getText());
                    }

                    $project = Project::whereTitle($title)->first();
                    if (!$project) {
                        $project = new Project();
                    }

                    $project->title = $title;
                    $project->link = $link;
                    $project->discord = $discord;
                    $project->twitter = $twitter;
                    $project->time = $time;
                    $project->till_the_mint = $tillTheMint;
                    $project->count = intval($count);
                    $project->price = floatval($price);
                    $project->currency = 'SOL';
                    $project->extras = $extras;
                    $project->date = $date ? "$year-$month-$day" : null;

                    $project->save();
                    dump('Saved ' . $project->id . ' project');
                }
            }
        }

        $driver->quit();
    }

    public function createDriverConnection()
    {
        $serverUrl = 'http://localhost:4444';
        $desiredCapabilities = DesiredCapabilities::chrome();
        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments(['headless']);
        $chromeOptions->addArguments(['--no-sandbox']);
        $chromeOptions->addArguments(['window-size=1920,1080']);
        $chromeOptions->addArguments(['--disable-dev-shm-usage']);
        $chromeOptions->addArguments(["--disable-system-timezone-automatic-detection", "--local-timezone"]);
        $chromeOptions->addArguments(['--verbose']);
        $chromeOptions->addArguments(["--whitelisted-ips=''"]);
        $chromeOptions->addArguments(['--disable-extensions']);
        $chromeOptions->addArguments(['--disable-gpu']);
        $chromeOptions->addArguments(['--enable-features=NetworkService,NetworkServiceInProcess']);
        $chromeOptions->addArguments(['user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36']);
        $desiredCapabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        $driver = RemoteWebDriver::create($serverUrl, $desiredCapabilities, 3600000,3600000);
//        $driver = RemoteWebDriver::create($serverUrl, DesiredCapabilities::chrome());

        return $driver;
    }
}
