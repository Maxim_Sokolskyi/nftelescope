<?php

namespace App\Console\Commands;

use App\Models\Project;
use App\Models\ProjectDiscordBots;
use App\Models\ProjectDiscordFollowers;
use App\Models\ProjectDiscordMessages;
use Cassandra\Keyspace;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Cookie;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use Illuminate\Console\Command;
ini_set('max_execution_time', 900);

class ScrapeDiscord extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:discord';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape discord';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dump('Started');

//        $driver = $this->createDriverConnection();
//        $driver->get('https://stackoverflow.com/users/login?ssrc=head&returnurl=https%3a%2f%2fstackoverflow.com%2f');
//        sleep(1);
//        $driver->findElement(WebDriverBy::cssSelector('button[data-oauthserver]'))->click();
//        sleep(2);
//        $driver->findElement(WebDriverBy::cssSelector('input'))->sendKeys(env('GOOGLE_EMAIL'));
//        sleep(1);
//        $driver->executeScript('document.elementFromPoint(840, 415).click();');
//        sleep(3);
//        $driver->findElement(WebDriverBy::cssSelector('input[type="password"]'))
//            ->sendKeys(env('GOOGLE_PASSWORD'));
//        sleep(1);
//        $driver->executeScript('document.elementFromPoint(840, 385).click();');
//
//        die();

        $projects = Project::whereNotNull('discord')->get();
//        $projects = Project::whereDiscord('https://discord.gg/8VTNmSkRWj')->get();
        foreach ($projects as $key => $project) {
            $driver = $this->createDriverConnection();
            dump("PROJECT $key goes");
            $project->discord = strpos($project->discord, 'https://') !== false ? $project->discord : 'https://' . $project->discord;
            $driver->get($project->discord);
//            $driver->get('https://twitter.com/CosmicApesNFT');
//        $driver->get('https://discord.com/channels/@me/628611748257333250');
            dump('Opened discord');
            sleep(2);
            $buttons = $driver->findElements(WebDriverBy::cssSelector('button'));
            $h3s = $driver->findElements(WebDriverBy::cssSelector('h3'));
            if (count($h3s) > 0 && ($h3s[0]->getText() == 'Приглашение недействительно' || $h3s[0]->getText() == 'Invite Invalid')) {
                dump("SKIPPED PROJECT $key");
                $driver->quit();
                continue;
            }

            $buttons = $driver->findElements(WebDriverBy::cssSelector('button'));
            if (count($buttons) > 1 && $buttons[1]->getText() == 'Already have an account?') {
                $driver->findElements(WebDriverBy::cssSelector('button'))[1]->click();
                sleep(1);
                $driver->findElements(WebDriverBy::cssSelector('input'))[0]
                    ->sendKeys(env('DISCORD_EMAIL'));
                $driver->findElements(WebDriverBy::cssSelector('input'))[1]
                    ->sendKeys(env('DISCORD_PASSWORD'));
                $driver->findElements(WebDriverBy::cssSelector('button'))[1]->click();
                sleep(8);
            }

            $beepBoops = $driver->findElements(WebDriverBy::cssSelector('h3 + div'));
            $beepBoops = count($beepBoops) > 0 ? $beepBoops[0]->getText() : null;
            if ($beepBoops == 'Beep boop. Boop beep?') {
                dump('BEEEP BOOP!!!');
                $driver->quit();
                die();
            }

            $buttons = $driver->findElements(WebDriverBy::cssSelector('button'));
            if (count($buttons) > 0 && ($buttons[0]->getText() == 'Принять приглашение' || $buttons[0]->getText() == 'Accept Invite')) {
                $buttons[0]->click();
                sleep(8);
            }

            $driver->executeScript("
            const timer = ms => new Promise(resolve => setTimeout(resolve, ms));
            window.isLoopActive = true;
            window.messagesAmount = 0;
            let currentDate = new Date();
            currentDate = new Date(currentDate - 20 * 60000);

            async function fetchMessagesAmount() {
                document.querySelectorAll('#channels div[data-dnd-name]:not([draggable])').forEach(async (item) => {
                    let itemCoordinates = item.getBoundingClientRect();
                    document.elementFromPoint(itemCoordinates.x + 10, itemCoordinates.y + 10).click();
                    console.log('Clicked');
                    if (document.querySelector('div[aria-modal=true]')) {
                        document.elementFromPoint(10, 10).click();
                    }

                    let isChatClosed = item.querySelector('svg > path').getAttribute('d') == 'M17 11V7C17 4.243 14.756 2 12 2C9.242 2 7 4.243 7 7V11C5.897 11 5 11.896 5 13V20C5 21.103 5.897 22 7 22H17C18.103 22 19 21.103 19 20V13C19 11.896 18.103 11 17 11ZM12 18C11.172 18 10.5 17.328 10.5 16.5C10.5 15.672 11.172 15 12 15C12.828 15 13.5 15.672 13.5 16.5C13.5 17.328 12.828 18 12 18ZM15 11H9V7C9 5.346 10.346 4 12 4C13.654 4 15 5.346 15 7V11Z';
//                    if (item.firstElementChild.firstElementChild.lastElementChild != null && item.firstElementChild.firstElementChild.lastElementChild.firstElementChild != null && item.firstElementChild.firstElementChild.lastElementChild.firstElementChild.firstElementChild.tagName == 'svg') {
                    if (!isChatClosed) {
    //                    for (let i = 0; i < 10; i++)  {
    //                        document.querySelector('ol[role=list]').parentElement.parentElement.scrollTop -= 3000;
    //                        await timer(2000);
    //                    }
                        console.log('Clickable');
                        document.querySelectorAll('ol[role=list] li').forEach(msg => {
                            if (msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling != null && msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling != null && msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild != null && msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.nextElementSibling != null && msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.nextElementSibling.firstElementChild != null && msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.nextElementSibling.firstElementChild.textContent.split(' — ').length > 1 && msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling != null && msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.nextElementSibling.firstElementChild.textContent.split(' — ')[1].includes('Сегодня')) {
                                console.log('TODAYs msg');
                                let time = msg.firstElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.nextElementSibling.firstElementChild.textContent.split(' — ')[1].split(', в ')[1];
                                let msgDate = new Date();
                                msgDate.setHours(time.split(':')[0]);
                                msgDate.setMinutes(time.split(':')[1]);
                                if (currentDate <= msgDate) {
                                     console.log('BINGO');
                                    window.messagesAmount++;
                                }
                            }
                        });
                    }
                });
            }

            fetchMessagesAmount().then(() => {
                window.isLoopActive = false;
            });
        ");
            $driver->wait()->until(
                function ($driver) {
                    return $driver->executeScript('return window.isLoopActive === false;');
                }
            );
            dump('working on members amount');
            $driver->executeScript("
            const timer = ms => new Promise(resolve => setTimeout(resolve, ms));
            window.isLoopActive = true;
            window.groups = [];
            window.membersAmount = 0;
            window.botsAmount = 0;
            let scrolledVal = 0;
            let scrolledBots = [];

            async function loopScrollToPageBottom() {
                await timer(2500);
                do {
//                    document.querySelectorAll('div[role=list] > h2').forEach(item => {
//                        if (window.groups[item.textContent.split(' — ')[0]] === undefined) window.membersAmount +=
//                            parseInt(item.textContent.split(' — ')[1]);
//                        window.groups[item.textContent.split(' — ')[0]] = parseInt(item.textContent.split(' — ')[1]);
//                    });

                    document.querySelectorAll('div[role=list] > div svg + span').forEach(item => {
                        let currentBotName = item.parentElement.previousElementSibling.firstElementChild.textContent;
                        if (!scrolledBots.includes(currentBotName)) window.botsAmount++;
                        scrolledBots.push(currentBotName);
                    });

                    document.querySelector('aside > div > div[role=list]').parentElement.scrollTop += 550;
                    scrolledVal += 550;
                    await timer(200);
                } while (parseInt(document.querySelector('aside > div > div[role=list]').parentElement.scrollTop) > (parseInt(scrolledVal) - 150));


                document.querySelector('header').click();
                await timer(500);
                document.querySelector('#guild-header-popout-leave').click()
                await timer(500);
                document.querySelectorAll('button[type=submit] > div').forEach(item => {
                    if (item.textContent == 'Покинуть сервер' || item.textContent == 'Leave the server') {
                        item.click();
                    }
                });
await timer(250);
//                document.querySelector('ul[data-jump-section] > div:first-child > div:first-child > span:first-child').click();
//                await timer(2500);
//                let meCoordinates = document.querySelector('ul[role=tree] > div:nth-child(2) > div:first-child').getBoundingClientRect();
//                document.elementFromPoint(meCoordinates.x + 20, meCoordinates.y + 5).click();
                await timer(1000);
                document.querySelector('div[aria-label=Друзья] div[aria-controls=all-tab]').click();
                await timer(250);
                document.querySelectorAll('div[role=list]')[1].querySelectorAll('div[role=listitem] div[role=img]').forEach(async (friend) => {
                    if (friend.nextElementSibling.firstElementChild.firstElementChild.textContent == 'Faraon') {
                        friend.nextElementSibling.firstElementChild.firstElementChild.click();
                        await timer(2000);
                        return true;
                    }
                });
            }

            loopScrollToPageBottom().then(async () => {
                window.isLoopActive = false;
            });"
            );
            try {
                $driver->wait()->until(
                    function ($driver) {
                        return $driver->executeScript('return window.isLoopActive === false;');
                    }
                );
            } catch (\Exception $e) {
                dump('!!!EXCEPTION');
                dump($e->getMessage());
                dump($e->getLine());
                $driver->quit();
                continue;
            }

            $textMsgSpans = $driver->findElements(WebDriverBy::cssSelector('main[aria-label] form > div > div:first-child > div >div:nth-child(3) span'));
            $textMsgSpan = $textMsgSpans[count($textMsgSpans) - 1];
            try {
                $textMsgSpan->click();
            } catch (\Exception $e) {}

            $driver->getKeyboard()->sendKeys($project->discord);
            $driver->getKeyboard()->sendKeys(WebDriverKeys::ENTER);

//            $sentMessages = $driver->findElements(WebDriverBy::cssSelector('ol[role=list] li'));
//            $lastMessage = $sentMessages[count($sentMessages) - 1];
            sleep(2);
            $amountOfServerMembers = $driver->executeScript("return document.querySelectorAll('ol[role=list] li')[document.querySelectorAll('ol[role=list] li').length - 1].querySelector('strong span:last-child') ? document.querySelectorAll('ol[role=list] li')[document.querySelectorAll('ol[role=list] li').length - 1].querySelector('strong span:last-child').textContent : 0;");
            $botsAmount = $driver->executeScript('return window.botsAmount;');
            $messagesAmount = $driver->executeScript('return window.messagesAmount;');
            $amountOfServerMembers = str_replace(' участников', '', $amountOfServerMembers);
            $amountOfServerMembers = intval(str_replace(' ', '', $amountOfServerMembers));

            $this->saveProjectAdditionalData(
                ProjectDiscordFollowers::class,
                $project->id,
                $amountOfServerMembers);
            $this->saveProjectAdditionalData(
                ProjectDiscordBots::class,
                $project->id,
                $botsAmount);
            $this->saveProjectAdditionalData(
                ProjectDiscordMessages::class,
                $project->id,
                $messagesAmount);

            dump($amountOfServerMembers, $botsAmount, $messagesAmount);
            $driver->quit();
        }

        return Command::SUCCESS;
    }

    public function saveProjectAdditionalData($additionalDataModel, $projectId, $data) {
        $additionalData = new $additionalDataModel();

        $additionalData->project_id = $projectId;
        $additionalData->amount = $data;

        $additionalData->save();
    }

    public function createDriverConnection()
    {
        $serverUrl = 'http://localhost:4444';
        $desiredCapabilities = DesiredCapabilities::chrome();
        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments(['headless']);
        $chromeOptions->addArguments(['--no-sandbox']);
        $chromeOptions->addArguments(['start-maximized']);
        $chromeOptions->setExperimentalOption("useAutomationExtension", false);
        $chromeOptions->setExperimentalOption("excludeSwitches", ["enable-automation"]);
        $chromeOptions->addArguments(['window-size=1920,1080']);
        $chromeOptions->addArguments(['--disable-dev-shm-usage']);
        $chromeOptions->addArguments(["--disable-system-timezone-automatic-detection", "--local-timezone"]);
        $chromeOptions->addArguments(['--verbose']);
        $chromeOptions->addArguments(["--whitelisted-ips=''"]);
        $chromeOptions->addArguments(['--disable-extensions']);
        $chromeOptions->addArguments(['--disable-gpu']);
        $chromeOptions->addArguments(['--enable-features=NetworkService,NetworkServiceInProcess']);
//        $chromeOptions->addArguments(['user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36']);
        $desiredCapabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        $driver = RemoteWebDriver::create($serverUrl, $desiredCapabilities, 3600000,3600000);
//        $driver = RemoteWebDriver::create($serverUrl, DesiredCapabilities::chrome());

        return $driver;
    }
}
