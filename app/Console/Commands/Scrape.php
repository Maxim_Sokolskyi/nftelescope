<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use App\Models\Score;
use Illuminate\Support\Facades\Log;
use function Couchbase\defaultDecoder;
use Carbon\Carbon;
use App\Models\Project;

class Scrape extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:twitters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function createDriverConnection()
    {
        $serverUrl = 'http://localhost:4444';
        $desiredCapabilities = DesiredCapabilities::chrome();
        $chromeOptions = new ChromeOptions();
//        $chromeOptions->addArguments(['headless']);
        $chromeOptions->addArguments(['--no-sandbox']);
        $chromeOptions->addArguments(['window-size=1920,1080']);
        $chromeOptions->addArguments(['--disable-dev-shm-usage']);
        $chromeOptions->addArguments(['--verbose']);
        $chromeOptions->addArguments(["--whitelisted-ips=''"]);
        $chromeOptions->addArguments(['--disable-extensions']);
        $chromeOptions->addArguments(['--disable-gpu']);
        $chromeOptions->addArguments(['--enable-features=NetworkService,NetworkServiceInProcess']);
        $chromeOptions->addArguments(['user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36']);
        $desiredCapabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        $driver = RemoteWebDriver::create($serverUrl, $desiredCapabilities, 3600000,3600000);
//        $driver = RemoteWebDriver::create($serverUrl, DesiredCapabilities::chrome());

        return $driver;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $driver = $this->createDriverConnection();
        $projects = Project::all();

        $newCommand = new \App\Models\Command();

        $scores = [];
        $followers = [];
        $twittersExceptions = [];
        $amountOfProjects = count($projects);
        foreach ($projects as $key => $project) {
            if (!$project->twitter) {
                continue;
            }

            $score = 0;
            $twitterGroupFollowers = 0;
            $newCommand->status = $key . ' / ' . $amountOfProjects;
            $newCommand->save();
            try {
                dump('-----');
                dump($key . ' / ' . $amountOfProjects);
                $twitterGroupData = $this->retrievePostScore($driver, $project->twitter);
                $score = $twitterGroupData[0];
                $twitterGroupFollowers = $twitterGroupData[1];
                dump($project->twitter . ' ' . $score . ' ' . $twitterGroupFollowers);
                dump('-----');
            } catch (\Exception $e) {
                dump($e->getLine() . ' ||| ' . $e->getMessage());
                $newCommand->log = $e->getLine() . ' ||| ' . $e->getMessage();
                $newCommand->save();
            }

            $projectScore = new Score();

            $projectScore->value = $score;
            $projectScore->followers = str_replace(',', '', $twitterGroupFollowers);
            $projectScore->timestamp = Carbon::now('UTC')->format('Y-m-d H:i:s');
            $projectScore->project_id = $project->id;

            $projectScore->save();

            if ($score == 0) {
                $twittersExceptions[] = $project;
            }
        }

        $driver->quit();

        return Command::SUCCESS;
    }

    public function retrievePostScore($driver, $twitter) {
        $score = 0;
        $driver->get($twitter);
        sleep(5);

        $followers = $driver->findElement(WebDriverBy::cssSelector("main > div > div > div > div > div > div:nth-child(2) > div > div > div > div:nth-child(2) > div:nth-child(5) > div:nth-child(2) > a > span:first-child > span"))
            ->getText();
        $followers = strpos($followers, 'тыс.') !== false ? floatval(str_replace(',', '.', $followers)) * 1000 : $followers;
        $followers = strpos($followers, 'K') !== false ? floatval(str_replace(',', '.', $followers)) * 1000 : $followers;
        dump($followers);
        $score += intval(str_replace(',', '', $followers));

        $posts = $driver->findElements(WebDriverBy::cssSelector('nav[role=navigation] + section[role=region] > h1 + div > div article[data-testid=tweet]'));
        $isPinned = false;
        foreach ($posts as $key => $post) {
            if (!$isPinned) {
                $pinnedText = $driver->executeScript("return document.querySelectorAll('nav[role=navigation] + section[role=region] > h1 + div > div article[data-testid=tweet]')[" . $key . "].querySelector('svg') ? document.querySelectorAll('nav[role=navigation] + section[role=region] > h1 + div > div article[data-testid=tweet]')[" . $key . "].querySelector('svg').parentElement.parentElement.textContent : null");

                if ($pinnedText == 'Pinned Tweet') {
                    $isPinned = true;
                    continue;
                }
            }

            if (($key == 6 && $isPinned) || ($key == 5 && !$isPinned)) {
                break;
            }

            $comments = $driver->executeScript("return document.querySelectorAll('nav[role=navigation] + section[role=region] > h1 + div > div article[data-testid=tweet]')[" . $key . "].querySelector('div[data-testid=reply]').textContent");
            $retweets = $driver->executeScript("return document.querySelectorAll('nav[role=navigation] + section[role=region] > h1 + div > div article[data-testid=tweet]')[" . $key . "].querySelector('div[data-testid=retweet]').textContent");
            $likes = $driver->executeScript("return document.querySelectorAll('nav[role=navigation] + section[role=region] > h1 + div > div article[data-testid=tweet]')[" . $key . "].querySelector('div[data-testid=like]').textContent");

            $comments = strpos($comments, 'K') !== false ? floatval($comments) * 1000 : $comments;
            $retweets = strpos($retweets, 'K') !== false ? floatval($retweets) * 1000 : $retweets;
            $likes = strpos($likes, 'K') !== false ? floatval($likes) * 1000 : $likes;

            $score += intval($comments);
            $score += intval($retweets) * 2;
            $score += intval($likes);

            dump($comments . ' ' . $retweets . ' ' . $likes);
        }

        return [
            $score,
            $followers
        ];
    }
}
