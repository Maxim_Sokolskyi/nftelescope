<?php

namespace App\Http\Controllers;

use App\Models\ProjectDiscordBots;
use App\Models\ProjectDiscordFollowers;
use App\Models\ProjectDiscordMessages;
use App\Models\Score;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Notice;
use App\Models\Project;

class HomeController extends Controller
{
    public function index() {
        if (!isset($_GET['action'])) {
            return view('index');

            $notice = Notice::orderBy('id', 'DESC')->first();

            $projects = Project::all();
            $projectsIds = [];
            foreach ($projects as $project) {
                $projectsIds[] = $project->id;
            }

            $projectsScoresData = Score::whereIn('project_id', $projectsIds)
                ->whereDate('created_at', '>=', Carbon::now()->subDays(2)->toDateString())
                ->get();

            $projectsMappedScores = [];
            $projectsMappedFollowers = [];
            foreach ($projectsScoresData as $projectScoreData) {
                $projectsMappedScores[$projectScoreData->project_id][] = $projectScoreData->value;
                $projectsMappedFollowers[$projectScoreData->project_id][] = $projectScoreData->followers;
            }

            $projectsScores = [];
            foreach ($projectsMappedFollowers as $projectId => $projectMappedFollowers) {
                $diff2h = '?';
                $diff12h = '?';
                $diff24h = '?';
                if (count($projectMappedFollowers) >= 2) {
                    $projectsScores[$projectId]['followersDifference']['2h'] = ($projectMappedFollowers[0] - $projectMappedFollowers[1]) / $projectMappedFollowers[1] * 100;
                }

                if (count($projectMappedFollowers) >= 6) {
                    $projectsScores[$projectId]['followersDifference']['12h'] = ($projectMappedFollowers[0] - $projectMappedFollowers[5]) / $projectMappedFollowers[5] * 100;
                }

                if (count($projectMappedFollowers) >= 12) {
                    $projectsScores[$projectId]['followersDifference']['24h'] = ($projectMappedFollowers[0] - $projectMappedFollowers[11]) / $projectMappedFollowers[11] * 100;
                }

                $projectsScores[$projectId]['score'] = end($projectsMappedScores[$projectId]);
                $projectsScores[$projectId]['followers'] = $projectMappedFollowers[0];
            }

            return view('book', [
                'notice' => $notice,
                'projects' => $projects,
                'projectsScores' => $projectsScores
            ]);
        }

        switch ($_GET['action']) {
            case 'update-notice':
                $notice = Notice::orderBy('id', 'DESC')->first();

                $notice->value = $_GET['notice'];

                $notice->save();
                break;

            case 'get-projects':
                return $this->getProjects();
        }
    }

    public function getProjects() {
        $projects = Project::get();

        $projectsDiscordFollowers = ProjectDiscordFollowers::get()
            ->groupBy('project_id');

        $projectsDiscordBots = ProjectDiscordBots::get()
            ->groupBy('project_id');

        $projectsDiscordMessages = ProjectDiscordMessages::get()
            ->groupBy('project_id');

        $data = [];
        foreach ($projects as $project) {
            $mintDate = null;
            if ($project->time !== 'No time specified yet.') {
                $mintDate = Carbon::createFromFormat('Y-m-d H:i', $project->date . ' ' . str_replace(' UTC', '', $project->time), 'Europe/Kiev');
            }

            $data[] = [
                'id' => $project->id,
                'link' => $project->link,
                'project' => $project->title,
                'points' => $project->id,
                'attendance' => [
                    'average' => 0,
                    'day' => 0,
                    'threeDays' => 0,
                    'week' => 0,
                ],
                'mint' => $mintDate ? $mintDate->format('d M Y, h:i:s') . ' UTC' : null,
                'amount' => $project->count,
                'price' => [
                    'price' => $project->price,
                    'usd' => $project->price * 2549.79,
                ],
                'discord' => [
                    'link' => $project->discord,
                    'nickname' => 'nftproject',
                    'subscribers' => isset($projectsDiscordFollowers[$project->id]) ? end($projectsDiscordFollowers[$project->id]) : 0,
                    'growth' => [
                        'twoHours' => -5,
                        'twelveHours' => 15,
                        'day' => 25,
                    ],
                    'messages' => isset($projectsDiscordMessages[$project->id]) ? end($projectsDiscordMessages[$project->id]) : 0,
                    'bots' => isset($projectsDiscordBots[$project->id]) ? end($projectsDiscordBots[$project->id]) : 0,
                ],
                'instagram' => [
                    'link' => '',
                    'subscribers' => 24302,
                    'nickname' => 'nftproject',
                    'growth' => [
                        'twoHours' => -5,
                        'twelveHours' => 15,
                        'day' => 25,
                    ],
                    'reposts' => 21555,
                    'repostsFromVerified' => 5000,
                ],
                'twitter' => [
                    'link' => '',
                    'subscribers' => 24302,
                    'nickname' => 'nftproject',
                    'growth' => [
                        'twoHours' => -5,
                        'twelveHours' => 15,
                        'day' => 25,
                    ],
                ],
                'description' => $project->extras,
                'showDropdown' => false
            ];
        }

        return $data;
    }
}
