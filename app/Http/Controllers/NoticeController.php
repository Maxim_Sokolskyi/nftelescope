<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPUnit\Framework\Error\Notice;

class NoticeController extends Controller
{
    public function update() {
        $notice = Notice::orderBy('id', 'DESC')->first();

        $notice->value = $_POST['notice'];

        $notice->save();
    }

    public function get() {
        $notice = Notice::orderBy('id', 'DESC')->first();

        return $notice;
    }
}
