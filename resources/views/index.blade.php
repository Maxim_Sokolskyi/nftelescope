<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="icon" href="{{ url('/public/images/coin.png') }}">
        <title>NFTelescope</title>
				<script src="{{ asset('/public/js/app.js') }}" defer></script>
				{{-- <script src="{{ asset('/js/app.js') }}" defer>localhost</script> --}}
				
				{{-- <link rel="stylesheet" href="{{ mix('/css/app.css') }}"> --}}
    </head>
    <body>
			<div id="app"></div>
    </body>
</html>
