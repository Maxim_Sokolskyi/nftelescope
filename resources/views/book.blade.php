<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>
<table>
    <div id="notices" class="mb-3">
        <label for="exampleFormControlTextarea1" class="form-label">Notices:</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">{{ $notice->value }}</textarea>
    </div>
    <style>
        #notices {
            width: 80%;
            margin: 10px auto;
        }
    </style>
    <?php echo '<style>' .
        '.howrare-table {' .
        'background: #fff;' .
        'border: none;' .
        'border-collapse: collapse;' .
        'color: #222;' .
        'display: table;' .
        'font-size: 8pt;' .
        'margin: 0 auto;' .
        'width: auto;' .
        '}' .
        '.howrare-table th {' .
        'background: #fff;' .
        'border: none;' .
        'border-bottom: #ccc 4px solid;' .
        'color: #222;' .
        'font-family: Verdana, Tahoma, Arial, sans-serif;' .
        'font-size: 8pt;' .
        'font-weight: bold;' .
        'padding: 8px 16px;' .
        'text-align: left;' .
        'white-space: nowrap;' .
        '}' .
        '.howrare-table td {' .
        'background: #fff;' .
        'border: none;' .
        'border-bottom: #ccc 1px solid;' .
        'color: #222;' .
        'font-family: Verdana, Tahoma, Arial, sans-serif;' .
        'font-size: 8pt;' .
        'font-weight: normal;' .
        'padding: 8px 16px;' .
        'text-align: left;' .
        '}' .
        '.howrare-table .howrare-table-num {' .
        'text-align: right;' .
        '}' .
        '.howrare-table td.howrare-table-num {' .
        'color: #666;' .
        'font-size: 80%;' .
        '}' .
        '.howrare-table td.howrare-table-url {' .
        'color: #08f;' .
        'font-size: 80%;' .
        '}' .
        '.howrare-table td.howrare-table-url a {' .
        'color: inherit;' .
        'cursor: pointer;' .
        'text-decoration: none;' .
        '}' .
        '.howrare-table a:hover,' .
        '.howrare-table a:active,' .
        '.howrare-table a:focus {' .
        'color: #c22 !important;' .
        'outline: none !important;' .
        'text-decoration: none !important;' .
        '}' .
        '.howrare-table td.howrare-table-discord {' .
        'color: #08f;' .
        'font-size: 80%;' .
        '}' .
        '.howrare-table td.howrare-table-discord a {' .
        'color: inherit;' .
        'cursor: pointer;' .
        'text-decoration: none;' .
        '}' .
        '.howrare-table td.howrare-table-twitter {' .
        'color: #08f;' .
        'font-size: 80%;' .
        '}' .
        '.howrare-table td.howrare-table-twitter a {' .
        'color: inherit;' .
        'cursor: pointer;' .
        'text-decoration: none;' .
        '}' .
        '.howrare-table .howrare-table-date {' .
        'text-align: right;' .
        '}' .
        '.howrare-table td.howrare-table-date {' .
        'font-size: 80%;' .
        '}' .
        '.howrare-table .howrare-table-mint {' .
        'text-align: right;' .
        '}' .
        '.howrare-table td.howrare-table-mint {' .
        'font-size: 80%;' .
        '}' .
        '.howrare-table .howrare-table-count {' .
        'text-align: right;' .
        '}' .
        '.howrare-table .howrare-table-price {' .
        'text-align: right;' .
        '}' .
        '.howrare-table td.howrare-table-extra {' .
        'color: #666;' .
        'font-size: 80%;' .
        '}' .
        '</style>';

    echo '<table class="howrare-table">' .
        '<thead>' .
        '<tr>' .
        '<th class="howrare-table-num">№</th>' .
        '<th class="howrare-table-name">проект</th>' .
        '<th class="howrare-table-url">сайт</th>' .
        '<th class="howrare-table-discord">дискорд</th>' .
        '<th class="howrare-table-twitter">твиттер</th>' .
        '<th class="howrare-table-date">дата</th>' .
        '<th class="howrare-table-mint">срок</th>' .
        '<th class="howrare-table-count">кол-во</th>' .
        '<th class="howrare-table-price">цена</th>' .
        '<th class="howrare-table-extra">детали</th>' .
        '<th class="howrare-table-extra">баллы</th>' .
        '<th class="howrare-table-extra">изменение баллов за 2ч</th>' .
        '<th class="howrare-table-extra">подписчики</th>' .
        '<th class="howrare-table-extra">изменение подписчиков за 2ч</th>' .
        '<th class="howrare-table-extra">изменение подписчиков за 12ч</th>' .
        '<th class="howrare-table-extra">изменение подписчиков за 24ч</th>' .
        '</tr>' .
        '</thead>' .
        '<tbody>';
    ?>
    @foreach ($projects as $key => $project)
        <tr>
            <td class="howrare-table-num">{{ $key }}</td>
            <td class="howrare-table-name">{{ $project->title }}</td>
            <td class="howrare-table-url">@if ($project->link)<a href="{{ $project->link }}" rel="nofollow noopener noreferrer" target="_blank">{{ preg_replace('~[/\\\\]+$~u', '', preg_replace('~^https?:[/\\\\][/\\\\](www\.)*~ui', '', $project->link))  }}</a>@endif</td>
            <td class="howrare-table-discord">@if ($project->discord)<a href="{{ $project->discord }}" rel="nofollow noopener noreferrer" target="_blank">{{ preg_replace('~[/\\\\]+$~u', '', preg_replace('~^https?:[/\\\\][/\\\\][^/\\\\]+[/\\\\](invite[/\\\\])?~ui', '@', $project->discord)) }}</a>@endif</td>
            <td class="howrare-table-twitter">@if ($project->twitter)<a href="{{ $project->twitter }}" rel="nofollow noopener noreferrer" target="_blank">{{ preg_replace('~[/\\\\]+$~u', '', preg_replace('~^https?:[/\\\\][/\\\\][^/\\\\]+[/\\\\](invite[/\\\\])?~ui', '@', $project->twitter)) }}</a>@endif</td>
            <td class="howrare-table-date" nowrap>{{ $project->date }}</td>
            <td class="howrare-table-mint">{{ $project->till_the_mint }}</td>
            <td class="howrare-table-count" nowrap>{{ $project->count }}</td>
            <td class="howrare-table-price" nowrap>{{ $project->price }}</td>
            <td class="howrare-table-extra">{{ $project->extras }}</td>
            <td class="howrare-table-extra">{{ isset($projectsScores[$project->id]['score']) ?? $projectsScores[$project->id]['score'] }}</td>
            <td class="howrare-table-extra">10%</td>
            <td class="howrare-table-extra">{{ isset($projectsScores[$project->id]['followers']) ?? $projectsScores[$project->id]['followers'] }}</td>
            <td class="howrare-table-extra">{{ isset($projectsScores[$project->id]['followersDifference']['2h']) ? $projectsScores[$project->id]['followersDifference']['2h'] . '%' : '' }}</td>
            <td class="howrare-table-extra">{{ isset($projectsScores[$project->id]['followersDifference']['12h']) ? $projectsScores[$project->id]['followersDifference']['12h'] . '%' : '' }}</td>
            <td class="howrare-table-extra">{{ isset($projectsScores[$project->id]['followersDifference']['24h']) ? $projectsScores[$project->id]['followersDifference']['24h'] . '%' : '' }}</td>
        </tr>
    @endforeach
    </table>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelector('#notices textarea').oninput = function () {
            $.ajax({
                url: '/?action=update-notice',
                type: 'GET',
                data: {
                    'notice': this.value
                },
                success: function () {}
            })
        }
    });
</script>
</body>
</html>
