<?php
$html = FALSE;
$handle = @ curl_init();
if ($handle !== FALSE) {
    $url = 'https://howrare.is/drops';
    $domain = 'howrare.is';
    @ curl_setopt($handle, CURLOPT_URL, $url);
    @ curl_setopt($handle, CURLOPT_REFERER, 'https://' . $domain . '/');
    @ curl_setopt($handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36');
    @ curl_setopt($handle, CURLOPT_HTTPHEADER, array(
        'Host: ' . $domain,
        'Connection: keep-alive',
        'User-Agent: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36',
        'Accept: text/html,text/xml,application/atom+xml,application/xhtml+xml,application/xml;q=0.9,image/webp,image/svg+xml,*/*;q=0.8',
        'Accept-Encoding: none',
        'Accept-Language: ru-RU,ru-UA,ru,uk-RU,uk-UA,uk;q=0.8,en-US;q=0.6,en;q=0.4'
    ));
    @ curl_setopt($handle, CURLOPT_COOKIE, 'q=; url=' . rawurlencode($url) . '; ls=1;');
    @ curl_setopt($handle, CURLOPT_HEADER, 1);
    @ curl_setopt($handle, CURLOPT_RETURNTRANSFER , 1);
    @ curl_setopt($handle, CURLOPT_FOLLOWLOCATION , 1);
    @ curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
    @ curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
    @ curl_setopt($handle, CURLOPT_TIMEOUT, 10);
    $html = @ curl_exec($handle);
    @ curl_close($handle);
} ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>
<body>
<div id="notices" class="mb-3">
    <label for="exampleFormControlTextarea1" class="form-label">Notices:</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3">{{ $notice->value }}</textarea>
</div>
<style>
    #notices {
        width: 80%;
        margin: 10px auto;
    }
</style>
<?php echo '<style>' .
    '.howrare-table {' .
    'background: #fff;' .
    'border: none;' .
    'border-collapse: collapse;' .
    'color: #222;' .
    'display: table;' .
    'font-size: 8pt;' .
    'margin: 0 auto;' .
    'width: auto;' .
    '}' .
    '.howrare-table th {' .
    'background: #fff;' .
    'border: none;' .
    'border-bottom: #ccc 4px solid;' .
    'color: #222;' .
    'font-family: Verdana, Tahoma, Arial, sans-serif;' .
    'font-size: 8pt;' .
    'font-weight: bold;' .
    'padding: 8px 16px;' .
    'text-align: left;' .
    'white-space: nowrap;' .
    '}' .
    '.howrare-table td {' .
    'background: #fff;' .
    'border: none;' .
    'border-bottom: #ccc 1px solid;' .
    'color: #222;' .
    'font-family: Verdana, Tahoma, Arial, sans-serif;' .
    'font-size: 8pt;' .
    'font-weight: normal;' .
    'padding: 8px 16px;' .
    'text-align: left;' .
    '}' .
    '.howrare-table .howrare-table-num {' .
    'text-align: right;' .
    '}' .
    '.howrare-table td.howrare-table-num {' .
    'color: #666;' .
    'font-size: 80%;' .
    '}' .
    '.howrare-table td.howrare-table-url {' .
    'color: #08f;' .
    'font-size: 80%;' .
    '}' .
    '.howrare-table td.howrare-table-url a {' .
    'color: inherit;' .
    'cursor: pointer;' .
    'text-decoration: none;' .
    '}' .
    '.howrare-table a:hover,' .
    '.howrare-table a:active,' .
    '.howrare-table a:focus {' .
    'color: #c22 !important;' .
    'outline: none !important;' .
    'text-decoration: none !important;' .
    '}' .
    '.howrare-table td.howrare-table-discord {' .
    'color: #08f;' .
    'font-size: 80%;' .
    '}' .
    '.howrare-table td.howrare-table-discord a {' .
    'color: inherit;' .
    'cursor: pointer;' .
    'text-decoration: none;' .
    '}' .
    '.howrare-table td.howrare-table-twitter {' .
    'color: #08f;' .
    'font-size: 80%;' .
    '}' .
    '.howrare-table td.howrare-table-twitter a {' .
    'color: inherit;' .
    'cursor: pointer;' .
    'text-decoration: none;' .
    '}' .
    '.howrare-table .howrare-table-date {' .
    'text-align: right;' .
    '}' .
    '.howrare-table td.howrare-table-date {' .
    'font-size: 80%;' .
    '}' .
    '.howrare-table .howrare-table-mint {' .
    'text-align: right;' .
    '}' .
    '.howrare-table td.howrare-table-mint {' .
    'font-size: 80%;' .
    '}' .
    '.howrare-table .howrare-table-count {' .
    'text-align: right;' .
    '}' .
    '.howrare-table .howrare-table-price {' .
    'text-align: right;' .
    '}' .
    '.howrare-table td.howrare-table-extra {' .
    'color: #666;' .
    'font-size: 80%;' .
    '}' .
    '</style>';
if (is_string($html)) {
$items = preg_replace('~^.*?</header>~uis', '', $html);
if ($items != $html) {
$html = preg_replace('~<footer(\s[^>]*)?>.*?$~uis', '', $items);
if ($items != $html) {
$items = preg_split('~<i\s+class="far\s+fa-calendar-alt">\s*</i>\s+~ui', $html);
if (!empty($items)) {
$projects = array();
foreach ($items as $html) {
    $pattern = '~^([a-z]+)\s([0-9]+)th\s*<.*$~uis';
    $month = preg_replace($pattern, '$1', $html);
    if ($month == $html) {
        $month = '';
        $day = '';
    } else {
        $day = preg_replace($pattern, '$2', $html);
        if ($day == $html) {
            $day = '?';
        }
    }
    $tbody = preg_replace('~^.*?<table(\s[^>]*)?>.*?<tbody(\s[^>]*)?>(.+?)</tbody>.*$~uis', '$3', $html);
    if ($tbody != $html) {
        $tbody = preg_replace('~&[^;]*;~u', ' ', $tbody);
        $tbody = preg_replace('~[^a-z0-9\.,:;!\?"=\'<>&\#/\\\\_\+\(\)-]+~ui', ' ', $tbody);
        $tbody = preg_replace('~\s+(style|class|target)="[^"]*"~ui', ' ', $tbody);
        $tbody = preg_replace('~\s+~u', ' ', $tbody);
        $tbody = preg_replace('~\s+([<>])~u', '$1', $tbody);
        $tbody = preg_replace('~([<>])\s+~u', '$1', $tbody);
        $tbody = preg_replace('~</(a|i|tr)>~u', '', $tbody);
        $tbody = preg_replace('~No\s+time\s+specified\s+yet\.?~ui', '', $tbody);
        $rows = preg_split('~<tr(\s[^>]*)?>~ui', $tbody);
        if (!empty($rows)) {
            $pattern = '~^<td\s+scope="row">([^<]+)</td><td>(.+?)</td><td>([^<]*)</td><td>([^<]*)</td><td>([^<]*)</td><td>([^<]*)</td>$~ui';
            foreach ($rows as $row) {
                $project = preg_replace($pattern, '$1', $row);
                if ($project != $row) {
                    $links = preg_replace($pattern, '$2', $row);
                    if ($links != $row) {
                        $pattern2 = '~^(.*?)<a href="(https?://(www\.)*discord\.(com|gg)/[^"]+)"(.*)$~ui';
                        $discord = preg_replace($pattern2, '$2', $links);
                        if ($discord == $links) {
                            $discord = '';
                        }
                        $links = preg_replace($pattern2, '$1$5', $links);
                        $pattern2 = '~^(.*?)<a href="(https?://(www\.)*twitter\.com/[^"]+)"(.*)$~ui';
                        $twitter = preg_replace($pattern2, '$2', $links);
                        if ($twitter == $links) {
                            $twitter = '';
                        }
                        $links = preg_replace($pattern2, '$1$4', $links);
                        $pattern2 = '~^.*?<a href="(https?://[^"]+)".*$~ui';
                        $url = preg_replace($pattern2, '$1', $links);
                        if ($url == $links) {
                            $url = '';
                        }
                        if (!empty($url) || !empty($discord) || !empty($twitter)) {
                            $mint = preg_replace($pattern, '$3', $row);
                            $count = preg_replace($pattern, '$4', $row);
                            $price = preg_replace($pattern, '$5', $row);
                            $extra = preg_replace($pattern, '$6', $row);
                            $index = strtolower($project);
                            if (!isset($projects[$index])) {
                                $projects[$index] = array(
                                    'day'     => $day,
                                    'month'   => $month,
                                    'name'    => $project,
                                    'url'     => $url,
                                    'discord' => $discord,
                                    'twitter' => $twitter,
                                    'mint'    => $mint,
                                    'count'   => $count,
                                    'price'   => $price,
                                    'extra'   => $extra
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}
if (!empty($projects)) {
$num = 1;
?>

<div id="notices" class="mb-3">
    <label for="exampleFormControlTextarea1" class="form-label">Example textarea</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
</div>

<?php echo '<table class="howrare-table">' .
                        '<thead>' .
                        '<tr>' .
                        '<th class="howrare-table-num">№</th>' .
                        '<th class="howrare-table-name">проект</th>' .
                        '<th class="howrare-table-url">сайт</th>' .
                        '<th class="howrare-table-discord">дискорд</th>' .
                        '<th class="howrare-table-twitter">твиттер</th>' .
                        '<th class="howrare-table-date">дата</th>' .
                        '<th class="howrare-table-mint">срок</th>' .
                        '<th class="howrare-table-count">кол-во</th>' .
                        '<th class="howrare-table-price">цена</th>' .
                        '<th class="howrare-table-extra">детали</th>' .
                        '<th class="howrare-table-extra">баллы</th>' .
                        '<th class="howrare-table-extra">изменение баллов за 2ч</th>' .
                        '<th class="howrare-table-extra">подписчики</th>' .
                        '<th class="howrare-table-extra">изменение подписчиков за 2ч</th>' .
                        '<th class="howrare-table-extra">изменение подписчиков за 12ч</th>' .
                        '<th class="howrare-table-extra">изменение подписчиков за 24ч</th>' .
                        '</tr>' .
                        '</thead>' .
                        '<tbody>';
                    $twitters = '';
                    $scores = \Illuminate\Support\Facades\DB::connection('mysql')->
                        select("select * from scores ORDER BY `id` DESC LIMIT 1");
                    $scores2 = \Illuminate\Support\Facades\DB::connection('mysql')->
                        select("select * from scores ORDER BY `created_at` DESC LIMIT 1 OFFSET 1");
                    $scores3 = \Illuminate\Support\Facades\DB::connection('mysql')->
                        select("select * from scores ORDER BY `created_at` DESC LIMIT 1 OFFSET 6");
                    $scores4 = \Illuminate\Support\Facades\DB::connection('mysql')->
                        select("select * from scores ORDER BY `created_at` DESC LIMIT 1 OFFSET 12");

                    $dataArray = json_decode($scores[0]->value);
                    $data2Array = json_decode($scores2[0]->value);
                    $data3Array = json_decode($scores3[0]->value);
                    $data4Array = json_decode($scores4[0]->value);

                    $iterator = -1;
                    foreach ($projects as $row) {
                        if (!empty($row['twitter'])) {
                            $iterator++;
                        }
                        if (!empty($row['twitter'])) {
                            $twitters .= $row['twitter'] . ' ';
                        }

                        $currentScore = '';
                        $currentScore2 = '';
                        $currentScore3 = '';
                        $currentScore4 = '';
                        $currentFollowers = '';
                        $currentFollowers2 = '';
                        $currentFollowers3 = '';
                        $currentFollowers4 = '';
                        if (!empty($row['twitter']) && property_exists($dataArray, $row['twitter']) && property_exists($data2Array, $row['twitter'])) {
                            $twitterUrl = $row['twitter'];
                            $currentScore = intval($dataArray->$twitterUrl->score);
                            $currentScore2 = $currentScore > 0 ? intval(intval($data2Array->$twitterUrl->score) / ($currentScore / 100) - 100) : 0;
                            $currentFollowers = intval(str_replace(',', '', $dataArray->$twitterUrl->followers));
                            $prevFollowers = intval(str_replace(',', '', $data2Array->$twitterUrl->followers));
                            $currentFollowers2 = $prevFollowers != 0 ? intval(($currentFollowers - $prevFollowers) / $prevFollowers * 100) : 0;
                        }

                        if (!empty($row['twitter']) && property_exists($dataArray, $row['twitter']) && property_exists($data3Array, $row['twitter'])) {
                            $twitterUrl = $row['twitter'];
                            $currentScore = intval($dataArray->$twitterUrl->score);
                            $currentScore3 = $currentScore > 0 ? intval(intval($data3Array->$twitterUrl->score) / ($currentScore / 100) - 100) : 0;
                            $currentFollowers = intval(str_replace(',', '', $dataArray->$twitterUrl->followers));
                            $prevFollowers = intval(str_replace(',', '', $data3Array->$twitterUrl->followers));
                            $currentFollowers3 = $prevFollowers != 0 ? intval(($currentFollowers - $prevFollowers) / $prevFollowers * 100) : 0;
                        }

                        if (!empty($row['twitter']) && property_exists($dataArray, $row['twitter']) && property_exists($data4Array, $row['twitter'])) {
                            $twitterUrl = $row['twitter'];
                            $currentScore = intval($dataArray->$twitterUrl->score);
                            $currentScore4 = $currentScore > 0 ? intval(intval($data4Array->$twitterUrl->score) / ($currentScore / 100) - 100) : 0;
                            $currentFollowers = intval(str_replace(',', '', $dataArray->$twitterUrl->followers));
                            $prevFollowers = intval(str_replace(',', '', $data4Array->$twitterUrl->followers));
                            $currentFollowers4 = $prevFollowers != 0 ? intval(($currentFollowers - $prevFollowers) / $prevFollowers * 100) : 0;
                        }

                        echo '<tr>' .
                            '<td class="howrare-table-num">' . $num . '</td>' .
                            '<td class="howrare-table-name">' . $row['name'] . '</td>' .
                            '<td class="howrare-table-url">'     . (!empty($row['url'])     ? '<a href="' . $row['url']     . '" rel="nofollow noopener noreferrer" target="_blank">' . preg_replace('~[/\\\\]+$~u', '', preg_replace('~^https?:[/\\\\][/\\\\](www\.)*~ui', '', $row['url']))                              . '</a>' : '') . '</td>' .
                            '<td class="howrare-table-discord">' . (!empty($row['discord']) ? '<a href="' . $row['discord'] . '" rel="nofollow noopener noreferrer" target="_blank">' . preg_replace('~[/\\\\]+$~u', '', preg_replace('~^https?:[/\\\\][/\\\\][^/\\\\]+[/\\\\](invite[/\\\\])?~ui', '@', $row['discord'])) . '</a>' : '') . '</td>' .
                            '<td class="howrare-table-twitter">' . (!empty($row['twitter']) ? '<a href="' . $row['twitter'] . '" rel="nofollow noopener noreferrer" target="_blank">' . preg_replace('~[/\\\\]+$~u', '', preg_replace('~^https?:[/\\\\][/\\\\][^/\\\\]+[/\\\\](invite[/\\\\])?~ui', '@', $row['twitter'])) . '</a>' : '') . '</td>' .
                            '<td class="howrare-table-date" nowrap>' . $row['day'] . ' ' . $row['month'] . '</td>' .
                            '<td class="howrare-table-mint">' . $row['mint'] . '</td>' .
                            '<td class="howrare-table-count" nowrap>' . $row['count'] . '</td>' .
                            '<td class="howrare-table-price" nowrap>' . $row['price'] . '</td>' .
                            '<td class="howrare-table-extra">' . $row['extra'] . '</td>' .
                            '<td class="howrare-table-extra">' . $currentScore . '</td>' .
                            '<td class="howrare-table-extra">' . $currentScore2 . '%</td>' .
                            '<td class="howrare-table-extra">' . $currentFollowers . '</td>' .
                            '<td class="howrare-table-extra">' . $currentFollowers2 . '%</td>' .
                            '<td class="howrare-table-extra">' . $currentFollowers3 . '%</td>' .
                            '<td class="howrare-table-extra">' . $currentFollowers4 . '%</td>' .
                            '</tr>';
                        $num++;
                    }

                    \Illuminate\Support\Facades\DB::connection('mysql')->
                    insert("insert into twitters (`name`) values ('{$twitters}')");

                    echo     '</tbody>' .
                        '</table>';
                } else {
                    echo 'Error: No projects at the source data!';
                }
            } else {
                echo 'Error: The source data are empty!';
            }
        } else {
            echo 'Error: Bad format-2 of the source data!';
        }
    } else {
        echo 'Error: Bad format-1 of the source data!';
    }
} else {
    echo 'Error: The source data are not accessible!';
}
?>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelector('#notices textarea').oninput = function () {
            $.ajax({
                url: '/?action=update-notice',
                type: 'GET',
                data: {
                    'notice': this.value
                },
                success: function () {}
            })
        }
    });
</script>
</body>
</html>
