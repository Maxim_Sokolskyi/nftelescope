import axios from 'axios'
import { apiUrls } from '../config/constants';

let axiosInstance = axios.create({
	baseURL: apiUrls.baseUrl
});

axiosInstance.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';

export default axiosInstance;
