export const data = [
	{
		id: 57478,
		link: '',
		project: 'NFT Project Live Game Club',
		points: 1200,
		attendance: {
			average: 24302,
			day: -5,
			threeDays: 15,
			week: 25
		},
		mint: '07 Feb 2022, 01:30:00 UTC',
		amount: 228,
		price: {
			ethereum: 0.4,
			usd: 194
		},
		discord: {
			link: '',
			nickname: 'nftproject',
			subscribers: 24302,
			growth: {
				twoHours: -5,
				twelveHours: 15,
				day: 25
			},
			messages: 255,
			bots: '2.55%'
		},
		instagram: {
			link: '',
			subscribers: 24302,
			nickname: 'nftproject',
			growth: {
				twoHours: -5,
				twelveHours: 15,
				day: 25
			},
			reposts: 21555,
			repostsFromVerified: 5000 
		},
		twitter: {
			link: '',
			subscribers: 24302,
			nickname: 'nftproject',
			growth: {
				twoHours: -5,
				twelveHours: 15,
				day: 25
			},
		},
		description: 'Описание проекта описание проекта описание проекта описание проекта описание проекта. $355 - $1200. Описание проекта описание проекта описание проекта описание проекта описание проекта. А также описание, но не проекта. Купил, продал тут описаниес 1200 до 20021 года.',
		showDropdown: false
	},
	{
		id: 57479,
		link: '',
		project: 'Bored Ape Yacht Club',
		points: 1300,
		attendance: {
			average: 24302,
			day: -5,
			threeDays: 15,
			week: 25
		},
		mint: '30 Jan 2022, 00:00:00 UTC',
		amount: 215,
		price: {
			ethereum: 7.5,
			usd: 2835
		},
		discord: {
			link: '',
			nickname: 'bored.ape',
			subscribers: 24302,
			growth: {
				twoHours: -5,
				twelveHours: 15,
				day: 25
			},
			messages: 355,
			bots: '0.55%'
		},
		instagram: {
			link: '',
			subscribers: 24302,
			nickname: 'bored.ape',
			growth: {
				twoHours: 5,
				twelveHours: -15,
				day: 25
			},
			reposts: 21555,
			repostsFromVerified: 5000 
		},
		twitter: {
			link: '',
			subscribers: 24302,
			nickname: 'bored.ape',
			growth: {
				twoHours: 5,
				twelveHours: 15,
				day: 25
			},
		},
		description: 'Описание проекта описание проекта описание проекта описание проекта описание проекта. $355 - $1200. Описание проекта описание проекта описание проекта описание проекта описание проекта. А также описание, но не проекта. Купил, продал тут описаниес 1200 до 20021 года.',
		showDropdown: false
	},
	{
		id: 57480,
		link: '',
		project: 'CryptoPunks',
		points: 1100,
		attendance: {
			average: 36710,
			day: 10,
			threeDays: 25,
			week: 20
		},
		mint: '19 Jan 2022, 00:00:00',
		amount: 238,
		price: {
			ethereum: 15.1,
			usd: 44420
		},
		discord: {
			link: '',
			nickname: 'CryptoPunks',
			subscribers: 36710,
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			messages: 255,
			bots: '2.55%'
		},
		instagram: {
			link: '',
			subscribers: 36710,
			nickname: 'CryptoPunks',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			reposts: 78455,
			repostsFromVerified: 7777 
		},
		twitter: {
			link: '',
			subscribers: 36710,
			nickname: 'CryptoPunks',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
		},
		description: 'Описание проекта описание проекта описание проекта описание проекта описание проекта. $355 - $1200. Описание проекта описание проекта описание проекта описание проекта описание проекта. А также описание, но не проекта. Купил, продал тут описаниес 1200 до 20021 года.',
		showDropdown: false
	},
	{
		id: 57481,
		link: '',
		project: 'FLUF World',
		points: 1150,
		attendance: {
			average: 36710,
			day: 10,
			threeDays: 25,
			week: 20
		},
		mint: '24 Jan 2022, 00:00:00 UTC',
		amount: 238,
		price: {
			ethereum: 10.2,
			usd: 33800
		},
		discord: {
			link: '',
			nickname: 'FLUF World',
			subscribers: 36710,
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			messages: 255,
			bots: '2.55%'
		},
		instagram: {
			link: '',
			subscribers: 36710,
			nickname: 'FLUF World',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			reposts: 78455,
			repostsFromVerified: 7777 
		},
		twitter: {
			link: '',
			subscribers: 36710,
			nickname: 'FLUF World',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
		},
		description: 'Описание проекта описание проекта описание проекта описание проекта описание проекта. $355 - $1200. Описание проекта описание проекта описание проекта описание проекта описание проекта. А также описание, но не проекта. Купил, продал тут описаниес 1200 до 20021 года.',
		showDropdown: false
	},
	{
		id: 57482,
		link: '',
		project: 'The Sandbox',
		points: 1100,
		attendance: {
			average: 36710,
			day: 10,
			threeDays: 25,
			week: 20
		},
		mint: '23 Jan 2022, 00:15:00 UTC',
		amount: 258,
		price: {
			ethereum: 11.4,
			usd: 36400
		},
		discord: {
			link: '',
			nickname: 'TheSandbox',
			subscribers: 36710,
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			messages: 255,
			bots: '2.55%'
		},
		instagram: {
			link: '',
			subscribers: 36710,
			nickname: 'TheSandbox',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			reposts: 78455,
			repostsFromVerified: 7777 
		},
		twitter: {
			link: '',
			subscribers: 36710,
			nickname: 'The_Sandbox',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
		},
		description: 'Описание проекта описание проекта описание проекта описание проекта описание проекта. $355 - $1200. Описание проекта описание проекта описание проекта описание проекта описание проекта. А также описание, но не проекта. Купил, продал тут описаниес 1200 до 20021 года.',
		showDropdown: false
	},
	{
		id: 57483,
		link: '',
		project: 'SolanaSluts',
		points: 900,
		attendance: {
			average: 36710,
			day: 10,
			threeDays: 25,
			week: 20
		},
		mint: '28 Jan 2022, 00:30:00 UTC',
		amount: 308,
		price: {
			ethereum: 4.74,
			usd: 7450
		},
		discord: {
			link: '',
			nickname: 'SolanaSluts',
			subscribers: 36710,
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			messages: 255,
			bots: '2.55%'
		},
		instagram: {
			link: '',
			subscribers: 36710,
			nickname: 'solana.sluts',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			reposts: 78455,
			repostsFromVerified: 7777 
		},
		twitter: {
			link: '',
			subscribers: 36710,
			nickname: 'SolanaSluts',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
		},
		description: 'Описание проекта описание проекта описание проекта описание проекта описание проекта. $355 - $1200. Описание проекта описание проекта описание проекта описание проекта описание проекта. А также описание, но не проекта. Купил, продал тут описаниес 1200 до 20021 года.',
		showDropdown: false
	},
	{
		id: 57483,
		link: '',
		project: 'Cool Cats NFT',
		points: 700,
		attendance: {
			average: 36710,
			day: 10,
			threeDays: 25,
			week: 20
		},
		mint: '23 Jan 2022, 00:30:00 UTC',
		amount: 297,
		price: {
			ethereum: 4.04,
			usd: 6350
		},
		discord: {
			link: '',
			nickname: 'SolanaSluts',
			subscribers: 36710,
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			messages: 255,
			bots: '2.55%'
		},
		instagram: {
			link: '',
			subscribers: 36710,
			nickname: 'solana.sluts',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			reposts: 78455,
			repostsFromVerified: 7777 
		},
		twitter: {
			link: '',
			subscribers: 36710,
			nickname: 'SolanaSluts',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
		},
		description: 'Описание проекта описание проекта описание проекта описание проекта описание проекта. $355 - $1200. Описание проекта описание проекта описание проекта описание проекта описание проекта. А также описание, но не проекта. Купил, продал тут описаниес 1200 до 20021 года.',
		showDropdown: false
	},
	{
		id: 57483,
		link: '',
		project: 'World of Woman',
		points: 650,
		attendance: {
			average: 36710,
			day: 10,
			threeDays: 25,
			week: 20
		},
		mint: '23 Jan 2022, 00:00:00 UTC',
		amount: 275,
		price: {
			ethereum: 3.54,
			usd: 5550
		},
		discord: {
			link: '',
			nickname: 'SolanaSluts',
			subscribers: 36710,
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			messages: 255,
			bots: '2.55%'
		},
		instagram: {
			link: '',
			subscribers: 36710,
			nickname: 'solana.sluts',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
			reposts: 78455,
			repostsFromVerified: 7777 
		},
		twitter: {
			link: '',
			subscribers: 36710,
			nickname: 'SolanaSluts',
			growth: {
				twoHours: 10,
				twelveHours: 25,
				day: 20
			},
		},
		description: 'Описание проекта описание проекта описание проекта описание проекта описание проекта. $355 - $1200. Описание проекта описание проекта описание проекта описание проекта описание проекта. А также описание, но не проекта. Купил, продал тут описаниес 1200 до 20021 года.',
		showDropdown: false
	}
]