const env = {
  production: "http://31.131.31.75/"
};

const apiUrls = {
	baseUrl: env.production,
	tokens: "?action=get-projects",
};

const paths = {
	public: window.location.origin.includes('127.0.0.1') ? '/' : '/public/'
}

export { apiUrls, paths }