import Home from './views/Home.vue'
import Login from './views/Login.vue'

export const routes = {
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/login',
			name: 'login',
			component: Login
		}
	],
}